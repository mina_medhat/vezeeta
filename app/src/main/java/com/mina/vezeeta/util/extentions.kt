package com.mina.vezeeta.util

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar


fun AppCompatActivity.navigateAndFinish(context: Context, cls: java.lang.Class<*>) {
    var i = Intent(this, cls)
    startActivity(i)
    finish()
}

fun AppCompatActivity.navigate(context: Context, cls: java.lang.Class<*>) {
    var i = Intent(this, cls)
    startActivity(i)

}




fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG) {
    val snack = Snackbar.make(this,message,Snackbar.LENGTH_LONG)
    snack.show()
}