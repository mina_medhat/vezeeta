package com.mina.vezeeta.util

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.mina.vezeeta.models.UserModel


class Prefs(context: Context) {
    private val CURRENT_LANGUAGE_KEY: String = "prefs.current_language"
    val PREFS_FILENAME = "com.mina.vezeeta"
    val LOGGED_IN_USER = "LOGGED_IN_USER"
    val USER_PREF = "USER_PREF"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);
    val gson = Gson()

    //my prefs
    var isLogedIn: Boolean
        get() = prefs.getBoolean(LOGGED_IN_USER, false)
        set(value) = prefs.edit().putBoolean(LOGGED_IN_USER, value).apply()

    var User: UserModel?
        get() = getUserPref()
        set(value) = setUserPref(value)

    private fun setUserPref(value: UserModel?) {
        if (value != null) {
            val json = gson.toJson(value)
            prefs.edit().putString(USER_PREF, json).apply()
        } else {
            prefs.edit().putString(USER_PREF, "").apply()
        }
    }

    private fun getUserPref(): UserModel? {
        val json = prefs.getString(USER_PREF, "")
        val userPref = gson.fromJson<UserModel>(json, UserModel::class.java)
        return userPref
    }
}