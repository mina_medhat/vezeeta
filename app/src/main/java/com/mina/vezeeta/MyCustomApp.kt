package com.mina.vezeeta.network

import android.app.Application
import com.mina.vezeeta.activities.main.di.offersModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

import com.mina.vezeeta.di.picassoModule
import com.mina.vezeeta.di.retrofitModule
import com.mina.vezeeta.ui.Details.di.OfferDetailsModule
import com.mina.vezeeta.ui.Login.di.loginModule
import com.mina.vezeeta.util.Prefs


class MyCustomApp : Application() {

    companion object {
        var prefs: Prefs? = null
    }

    override fun onCreate() {
        super.onCreate()
        prefs = Prefs(applicationContext)
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@MyCustomApp)
            modules(listOf(retrofitModule, picassoModule, offersModule,loginModule, offersModule,OfferDetailsModule))
        }
    }
}