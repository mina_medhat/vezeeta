package com.mina.vezeeta.ui.Login

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.mina.vezeeta.R

import com.mina.vezeeta.activities.main.viewModel.LoginViewModel
import com.mina.vezeeta.ui.ListOffersActivity
import com.mina.vezeeta.util.navigateAndFinish

import com.mina.vezeeta.util.snack
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {
    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {
            loginViewModel.login(txt_login_email.text.toString(), txt_login_pass.text.toString())
        }

        loginViewModel.uiState.observe(this, Observer {
            val dataState = it ?: return@Observer
            progress_bar.visibility = if (dataState.showProgress) View.VISIBLE else View.GONE
            if (dataState.loginSuccess != null && !dataState.loginSuccess.consumed)
                dataState.loginSuccess.consume()?.let {
                    if (it) {
                        naviagteToHome();
                    }
                }
            if (dataState.error != null && !dataState.error.consumed)
                dataState.error.consume()?.let { errorResource ->
                    Toast.makeText(this, resources.getString(errorResource), Toast.LENGTH_SHORT)
                        .show()

                    frm_login_content.snack(resources.getString(errorResource))
                    // handle error state
                }
        })
    }

    private fun naviagteToHome() {
        navigateAndFinish(this, ListOffersActivity::class.java)
    }
}
