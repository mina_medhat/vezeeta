package com.mina.vezeeta.activities.main.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mina.vezeeta.models.Offer
import com.mina.vezeeta.network.ServiceUtil
import com.mina.vezeeta.ui.List.Search.OfferSearchDataSourceFactory
import com.mina.vezeeta.ui.List.paging.OfferDataSource
import com.mina.vezeeta.ui.List.paging.OfferDataSourceFactory
import com.mina.vezeeta.util.Event

class ListViewModel (val serviceUtil: ServiceUtil) : ViewModel() {

    var itemPagedList: LiveData<PagedList<Offer>>
    var liveDataSource: LiveData<OfferDataSource>
    var filterTextAll = MutableLiveData<String>()

    init {
        val offerDataSourceFactory = OfferDataSourceFactory(serviceUtil)
        liveDataSource = offerDataSourceFactory.offerLiveDataSource
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(OfferDataSource.PAGE_SIZE)
            .build()
        itemPagedList = LivePagedListBuilder(offerDataSourceFactory, config)
            .build()
    }

    fun initAllServices() {
        itemPagedList = Transformations.switchMap(
            filterTextAll
        ) { input: String? ->
            if (input == null || input == "" || input == "%%") {
                val offerDataSourceFactory = OfferDataSourceFactory(serviceUtil)
                liveDataSource = offerDataSourceFactory.offerLiveDataSource
                val config = PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setPageSize(OfferDataSource.PAGE_SIZE)
                    .build()
                return@switchMap LivePagedListBuilder(offerDataSourceFactory, config)
                    .build()
            } else {
                val offerSearchDataSourceFactory = OfferSearchDataSourceFactory(input,serviceUtil)
                var liveSearchDataSource = offerSearchDataSourceFactory.offerLiveDataSource
                val config = PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setPageSize(DEFAULT_BUFFER_SIZE)
                    .build()
                return@switchMap LivePagedListBuilder(offerSearchDataSourceFactory, config)
                    .build()
            }
        }
    }
}



data class OffersDataState(
    val showProgress: Boolean,
    val error: Event<Int>?
)