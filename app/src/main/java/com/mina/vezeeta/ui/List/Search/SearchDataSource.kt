package com.mina.vezeeta.ui.List.Search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PageKeyedDataSource
import com.mina.vezeeta.R
import com.mina.vezeeta.activities.main.viewModel.OffersDataState
import com.mina.vezeeta.models.Offer
import com.mina.vezeeta.network.ServiceUtil
import com.mina.vezeeta.util.Event
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Created on 6/18/2020 in Vezeeta.
 * copyright mina
 */
class SearchDataSource(var text : String,private val serviceUtil: ServiceUtil) : PageKeyedDataSource<Int, Offer>() {

    private val _uiState = MutableLiveData<OffersDataState>()
    val uiState: LiveData<OffersDataState> get() = _uiState

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Offer>) {


    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Offer>
    ) {

        GlobalScope.launch {
            runCatching {
                emitUiState(showProgress = true)
                serviceUtil.search(text)
            }.onSuccess {
                callback.onResult(it.offersList, null,1);
                emitUiState(showProgress = false)

            }.onFailure {
                it.printStackTrace()
                emitUiState(error = Event(R.string.internet_failure_error))
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Offer>) {


    }

    private fun emitUiState(
        showProgress: Boolean = false,
        error: Event<Int>? = null
    ) {
        val dataState = OffersDataState(showProgress, error)
        _uiState.postValue(dataState)
    }

    companion object {
        const val PAGE_SIZE = 20
        const val FIRST_PAGE = 1
    }
}