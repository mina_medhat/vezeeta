package com.mina.vezeeta.activities.main.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import com.mina.vezeeta.activities.main.viewModel.ListViewModel


val offersModule = module {

    viewModel {
        ListViewModel(get())
    }

}