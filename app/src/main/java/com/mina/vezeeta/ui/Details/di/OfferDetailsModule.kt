package com.mina.vezeeta.ui.Details.di

import com.mina.vezeeta.ui.Details.viewModel.OfferDetailsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module



val OfferDetailsModule = module {

    viewModel {
        OfferDetailsViewModel(get())
    }

}