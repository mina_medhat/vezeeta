package com.mina.vezeeta.ui.Details

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.denzcoskun.imageslider.models.SlideModel
import com.mina.vezeeta.R
import com.mina.vezeeta.models.OfferDetails
import com.mina.vezeeta.ui.Details.viewModel.OfferDetailsViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*

import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel




class DetailsActivity : AppCompatActivity() {

    private val offerDetailsViewModel: OfferDetailsViewModel by viewModel()


    companion object {
        public val OFFER_KEY = "offer_key";
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val offerKey = intent.getStringExtra(OFFER_KEY);
        offerDetailsViewModel.getOfferDetails(offerKey);

        offerDetailsViewModel.uiState.observe(this, Observer {
            val dataState = it ?: return@Observer
            details_progress.visibility = if (dataState.showProgress) View.VISIBLE else View.GONE
            if (dataState.offerDetails != null && !dataState.offerDetails.consumed)
                dataState.offerDetails.consume()?.let { offer ->
                    updateView(offer);
                }
            if (dataState.error != null && !dataState.error.consumed)
                dataState.error.consume()?.let { errorResource ->
                    Toast.makeText(this, resources.getString(errorResource), Toast.LENGTH_SHORT)
                        .show()
                    // handle error state
                }
        })
    }

    private fun updateView(offer: OfferDetails) {
        val imageList = ArrayList<SlideModel>()
        offer.images.forEach {
            imageList.add(SlideModel(it))
        }
        img_slider_details.setImageList(imageList)

        txt_details_offer_name.text= offer.offerName
        txt_details_offer_info.text= offer.desc
    }
}
