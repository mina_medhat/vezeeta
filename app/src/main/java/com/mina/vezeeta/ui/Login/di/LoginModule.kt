package com.mina.vezeeta.ui.Login.di
import com.mina.vezeeta.activities.main.viewModel.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {

    viewModel {
        LoginViewModel(get())
    }

}