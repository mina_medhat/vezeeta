package com.mina.vezeeta.ui;

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.mina.vezeeta.R
import com.mina.vezeeta.activities.main.viewModel.ListViewModel
import com.mina.vezeeta.models.Offer
import com.mina.vezeeta.ui.Details.DetailsActivity
import com.mina.vezeeta.ui.List.paging.ItemAdapter
import com.mina.vezeeta.util.snack
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_list.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListOffersActivity : AppCompatActivity() {

    private val listViewModel: ListViewModel by viewModel()
    private val picasso: Picasso by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        listViewModel.initAllServices()
        val adapter = ItemAdapter(
            this,
            picasso, object : ItemAdapter.ListAdapterActions {
                override fun onBookClick(offer: Offer) {
                    var i = Intent(this@ListOffersActivity, DetailsActivity::class.java)
                    i.putExtra(DetailsActivity.OFFER_KEY,offer.offerKey);
                    startActivity(i)
                }
            })
        recycler_view.layoutManager = LinearLayoutManager(this)
        listViewModel.itemPagedList.observe(this, Observer {
            adapter.submitList(it)
            main_progress.visibility = View.GONE
            swref_list.setRefreshing(false);

        })
        recycler_view.adapter = adapter
        listViewModel.filterTextAll.setValue("");
        swref_list.setOnRefreshListener {
            listViewModel.liveDataSource.value?.invalidate();
        }
        swref_list.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        );

//        val adapter = (picasso)
//        recycler_view.apply {
//            layoutManager = GridLayoutManager(this@ListDoctorsActivity, 2)
//            addItemDecoration(GridSpacingItemDecoration(2, 30, true))
//            this.adapter = adapter
//        }
//
        listViewModel.liveDataSource.value?.uiState?.observe(this, Observer {
            val dataState = it ?: return@Observer
            main_progress.visibility = if (dataState.showProgress) View.VISIBLE else View.GONE
            if (dataState.error != null && !dataState.error.consumed)
                dataState.error.consume()?.let { errorResource ->
                    ln_list_content.snack(getString(errorResource));
                    // handle error state
                }
        })

        (search_list as SearchView).setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                main_progress.visibility = View.VISIBLE
                listViewModel.filterTextAll.postValue(query);
                return false;
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false;
            }

        })

    }
}
