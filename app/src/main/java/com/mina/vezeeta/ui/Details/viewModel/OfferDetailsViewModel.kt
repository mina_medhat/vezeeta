 package com.mina.vezeeta.ui.Details.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mina.vezeeta.R
import com.mina.vezeeta.models.OfferDetails
import com.mina.vezeeta.network.ServiceUtil
import com.mina.vezeeta.util.Event
import kotlinx.coroutines.launch

class OfferDetailsViewModel constructor(private val serviceUtil: ServiceUtil) : ViewModel() {

    private val _uiState = MutableLiveData<OfferDetailsDataState>()
    val uiState: LiveData<OfferDetailsDataState> get() = _uiState


    public fun getOfferDetails(key : String) {
        viewModelScope.launch {
            runCatching {
                emitUiState(showProgress = true)
                serviceUtil.offerDetails(key)
            }.onSuccess {
                emitUiState(movies = Event(it))
            }.onFailure {
                it.printStackTrace()
                emitUiState(error = Event(R.string.internet_failure_error))
            }
        }
    }

    private fun emitUiState(
        showProgress: Boolean = false,
        movies: Event<OfferDetails>? = null,
        error: Event<Int>? = null
    ) {
        val dataState = OfferDetailsDataState(showProgress, movies, error)
        _uiState.value = dataState
    }
}

data class OfferDetailsDataState(
    val showProgress: Boolean,
    val offerDetails: Event<OfferDetails>?,
    val error: Event<Int>?
)