package com.mina.vezeeta.ui.List.paging


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mina.vezeeta.R
import com.mina.vezeeta.models.Offer
import com.squareup.picasso.Picasso


/**
 * Created on 6/18/2020 in Vezeeta.
 * copyright mina
 */
class ItemAdapter(
    val mCtx: Context,
    val picasso: Picasso,
    val listAdapterActions: ListAdapterActions
) :
    PagedListAdapter<Offer, ItemAdapter.ItemViewHolder?>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemViewHolder {
        val view: View =
            LayoutInflater.from(mCtx).inflate(R.layout.single_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ItemViewHolder,
        position: Int
    ) {
        val item = getItem(position)
        if (item != null) {
            holder.textView.setText(item.offerName)
            picasso
                .load(item.mainImage)
                .into(holder.imageView)
            holder.btnBook.setOnClickListener {
                listAdapterActions.onBookClick(item)
            }
        } else {
            Toast.makeText(mCtx, "Item is null", Toast.LENGTH_LONG).show()
        }
    }

    inner class ItemViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var textView: TextView
        var imageView: ImageView
        var btnBook: Button

        init {
            textView = itemView.findViewById(R.id.offer_name_text_view)
            imageView = itemView.findViewById(R.id.offer_poster_image_view)
            btnBook = itemView.findViewById(R.id.btn_list_item_book)
        }
    }

    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<Offer> =
            object : DiffUtil.ItemCallback<Offer>() {
                override fun areItemsTheSame(
                    oldItem: Offer,
                    newItem: Offer
                ): Boolean {
                    return oldItem.offerKey === newItem.offerKey
                }

                override fun areContentsTheSame(
                    oldItem: Offer,
                    newItem: Offer
                ): Boolean {
                    return oldItem == newItem
                }
            }
    }

    interface ListAdapterActions {
        fun onBookClick(offer: Offer);
    }

}