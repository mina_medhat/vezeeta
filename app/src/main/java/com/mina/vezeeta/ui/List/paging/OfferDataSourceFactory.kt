package com.mina.vezeeta.ui.List.paging

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mina.vezeeta.activities.main.viewModel.OffersDataState
import com.mina.vezeeta.models.Offer
import com.mina.vezeeta.network.ServiceUtil
import com.mina.vezeeta.util.Event

/**
 * Created on 6/19/2020 in Vezeeta.
 *
 */
class OfferDataSourceFactory (val serviceUtil: ServiceUtil): DataSource.Factory<Int, Offer>() {
    val offerLiveDataSource = MutableLiveData<OfferDataSource>()


    override fun create(): DataSource<Int, Offer> {
        val userDataSource = OfferDataSource(serviceUtil);
        offerLiveDataSource.postValue(userDataSource)

        return userDataSource
    }
}
