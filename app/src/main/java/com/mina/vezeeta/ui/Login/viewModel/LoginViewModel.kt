package com.mina.vezeeta.activities.main.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mina.vezeeta.R
import com.mina.vezeeta.models.LoginResponse
import com.mina.vezeeta.network.ServiceUtil
import com.mina.vezeeta.models.UserModel
import com.mina.vezeeta.models.UserSingleton
import com.mina.vezeeta.network.MyCustomApp
import com.mina.vezeeta.util.Event
import kotlinx.coroutines.launch

class LoginViewModel constructor(private val serviceUtil: ServiceUtil) : ViewModel() {

    private val _uiState = MutableLiveData<LoginDataState>()
    val uiState: LiveData<LoginDataState> get() = _uiState

    var email: String? = null;
    var pass: String? = null;

    init {

    }

    fun login(email: String, password: String) {
        //validating email and password
        this.email = email
        this.pass = password
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            emitUiState(error = Event(R.string.empty_fileds))
            return
        } else {
            if (!UserModel.isEmailValid(email!!)) {
                emitUiState(error = Event(R.string.email_invalid))
                return
            }

            if (!UserModel.isPasswordLengthGreaterThan5(password!!)) {
                emitUiState(error = Event(R.string.pass_invalid))
                return
            }
        }

        viewModelScope.launch {
            runCatching {
                emitUiState(showProgress = true)
                serviceUtil.login(email!!, password!!)
            }.onSuccess {
                createAndCashUser(it);
                it.userToken?.let {
                    emitUiState(loginSuccess = Event<Boolean>(true))
                }

            }.onFailure {
                it.printStackTrace()
                emitUiState(error = Event(R.string.internet_failure_error))
            }
        }
    }

    private fun createAndCashUser(response: LoginResponse) {
        var user  = UserModel(email = email!! , pass = pass!!)
        user.userToken = response.userToken
        user.location = response.location
        user.name = response.name
        user.userKey = response.userKey
        user.userName = response.userName

        //cashting the user
        MyCustomApp.prefs!!.User = user;
        //add user singleton
        UserSingleton.instance = user
        //todo mina : we can use user.with to apply the above 2 instruction
    }

    private fun emitUiState(
        showProgress: Boolean = false,
        loginSuccess: Event<Boolean>? = null,
        error: Event<Int>? = null
    ) {
        val dataState = LoginDataState(showProgress, loginSuccess, error)
        _uiState.value = dataState
    }
}

data class LoginDataState(
    val showProgress: Boolean,
    val loginSuccess: Event<Boolean>?,
    val error: Event<Int>?
)