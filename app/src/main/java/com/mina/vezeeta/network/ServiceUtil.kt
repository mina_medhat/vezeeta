package com.mina.vezeeta.network

import com.mina.vezeeta.models.LoginResponse
import com.mina.vezeeta.models.OfferDetails
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import com.mina.vezeeta.models.OfferModel


interface ServiceUtil {

    @GET(value = "dummyOffers")
    suspend fun getOffers(
        @Query(
            value = "page"
        ) page: Int
    ): OfferModel

    @POST(value = "dummyLogin")
    suspend fun login(
        @Query(
            value = "email",
            encoded = true
        ) email: String, @Query(value = "password") password: String
    ): LoginResponse

    @GET(value = "dummyOffersSearch")
    suspend fun search(
        @Query(
            value = "text"
        ) search: String
    ): OfferModel

    @GET(value = "dummyOfferDetails")
    suspend fun offerDetails(
        @Query(
            value = "offerKey"
        ) search: String
    ): OfferDetails
}