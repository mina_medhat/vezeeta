package com.mina.vezeeta.models

/**
 * Created on 6/18/2020 in Vezeeta.
 * copyright mina
 */
data class OfferModel(
    val offersList: List<Offer>
)

data class Offer(
    val mainImage: String,
    val offerKey: String,
    val offerName: String
)