package com.mina.vezeeta.models

import android.util.Patterns


data class UserModel(var email: String, var pass: String) {
    var  userToken : String? = null
    var  userKey : String? = null
    var  name : String? = null
    var  userName : String? = null
    var  location : String? = null

    companion object {
        fun isEmailValid(email : String): Boolean {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        fun isPasswordLengthGreaterThan5(pass:String): Boolean {
            return pass.length > 5
        }
    }
}

object UserSingleton {
    var instance : UserModel? = null
}