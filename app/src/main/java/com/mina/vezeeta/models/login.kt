package com.mina.vezeeta.models

import kotlinx.serialization.Serializable


@Serializable
data class LoginResponse(
    val location: String,
    val name: String,
    val userKey: String,
    val userName: String,
    val userToken: String
)