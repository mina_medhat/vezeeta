package com.mina.vezeeta.models

/**
 * Created on 6/20/2020 in Vezeeta.
 * copyright mina
 */
data class OfferDetails(
    val desc: String,
    val images: List<String>,
    val offerName: String
)